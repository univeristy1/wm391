import cv2

original_image = cv2.imread('/Users/dylangill/Documents/University/WM391/CBSD68-dataset/CBSD68/original_png/0000.png')

# Read the image
noisy_image = cv2.imread('/Users/dylangill/Documents/University/WM391/CBSD68-dataset/CBSD68/noisy50/0000.png')

# Apply bilateral filter
filtered_image = cv2.bilateralFilter(noisy_image, d=9 , sigmaColor=75, sigmaSpace=75)

# Display the original and filtered images
cv2.imshow('Original Image', original_image)
cv2.imshow('Noisy Image', noisy_image)
cv2.imshow('Filtered Image', filtered_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
