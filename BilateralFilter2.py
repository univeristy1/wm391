import cv2
import numpy as np


def calculate_clarity(image):
    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Apply Laplacian operator to compute image clarity
    clarity = cv2.Laplacian(gray, cv2.CV_64F).var()
    return clarity


def calculate_noise(image):
    # Convert image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # Calculate standard deviation of pixel values as a measure of noise
    noise_score = np.std(gray)
    return noise_score


def main():
    # Read the original image
    original_image = cv2.imread('/Users/dylangill/Documents/University/WM391/CBSD68-dataset/CBSD68/original_png/0000.png')

    # Read the noisy image
    noisy_image = cv2.imread('/Users/dylangill/Documents/University/WM391/CBSD68-dataset/CBSD68/noisy50/0000.png')

    # Apply bilateral filter to the noisy image
    filtered_image = cv2.bilateralFilter(noisy_image, d=15, sigmaColor=75, sigmaSpace=75)

    # Calculate clarity and noise level for each image
    clarity_original = calculate_clarity(original_image)
    noise_level_original = calculate_noise(original_image)

    clarity_noisy = calculate_clarity(noisy_image)
    noise_level_noisy = calculate_noise(noisy_image)

    clarity_filtered = calculate_clarity(filtered_image)
    noise_level_filtered = calculate_noise(filtered_image)

    # Print the clarity and noise level values for each image
    print("Original Image - Clarity:", clarity_original, "Noise Level:", noise_level_original)
    print("Noisy Image - Clarity:", clarity_noisy, "Noise Level:", noise_level_noisy)
    print("Filtered Image - Clarity:", clarity_filtered, "Noise Level:", noise_level_filtered)

    # Display the original, noisy, and filtered images
    cv2.imshow('Original Image', original_image)
    cv2.imshow('Noisy Image', noisy_image)
    cv2.imshow('Filtered Image', filtered_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
